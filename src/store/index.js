import Vue from 'vue'
// import {risks} from '../api'
import axios from 'axios';

export default {
  state: {
    risk: {
      id: null,
      name: null,
      description: null,
      location: null,
      ownerid: 0,
      status: null,
      createdDate: null,
      nextReviewDate: null,
      reviewInterval: null,
      pastReviews: null,
      score: null,
      strategicObjective: null,
      currentControl: null
    },
    search: '',
    foundRisks: [],
    riskList: [],
    lastFetched: null, // Timestamp
    byID: [], // Lookup for Users
    isFetching: false,
    errorMessage: null,
    lastSearch: {}
  },
  getters: {
    risk: state => {
        return state.risk
    },
    riskList: state => {
      return state.riskList ? state.riskList : null
      // return state.users ? state.users : null
    },
  },
  mutations: {
    risksLoadStart(state) {
      state.errorMessage = null
      state.isFetching = true
    },
    async risksLoadSuccess(state, things){
        const byID = await things.reduce((acc, val) => {
        acc[val.id] = val
        return acc
        }, {})
        Vue.set(state, 'riskList', byID)
        state.lastFetched = Date.now()
        state.isFetching = false
        // risksLoad(this.$store.state, things)
    },
    risksLoadFailure(state, error) {
        state.errorMessage = error
        state.isFetching = false
    },
    riskList(state, risks) {
      state.riskList = risks
    }
  },
  actions: {
    loadRisks ({commit}) {
      commit('risksLoadStart')
      try {
        axios.get('risk').then((response) => {
        commit('riskList', response.data)
      })
    } catch (error) {
      commit('risksLoadFailure', error)
    }
    }
    // async risksLoad (context) {
    //   context.commit(`risksLoadStart`)
    //   try {
    //     const things = await risks('GET', {})
    //     context.commit(`risksLoadSuccess`, things)
    //     // eslint-disable-next-line
    //     console.log('POST commit things = ')
    //     // eslint-disable-next-line
    //     console.log(things)        
    //   } catch (error) {
    //     context.commit(`risksLoadFailure`, error.message)
    //     throw error
    //   }
    // }

  }
}
