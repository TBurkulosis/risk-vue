import Vue from 'vue'
import Vuex from 'vuex'
import Bulma from 'bulma'
import Buefy from 'buefy'
import App from './App.vue'
import router from './router'
// import store from './store'
import 'buefy/dist/buefy.css'
import axios from 'axios'
// import api from './api'

Vue.config.productionTip = false
Vue.use(Bulma)
Vue.use(Buefy)
Vue.use(Vuex)
Vue.use(require('vue-moment'))


axios.defaults.baseURL = 'https://localhost:5001/api'

const store = new Vuex.Store({
  state: {
    risk: {
      id: null,
      name: null,
      description: null,
      location: null,
      ownerid: 0,
      status: null,
      createdDate: null,
      nextReviewDate: null,
      reviewInterval: null,
      pastReviews: null,
      score: null,
      strategicObjective: null,
      currentControl: null
    },
    riskList: [],
    isFetching: true,
    errorMessage: null,
  },
  getters: {
    risk: state => {
      return state.risk
    },
    riskList: state => {
      return state.riskList
    }
  },
  mutations: {
    risksLoadStart(state) {
      state.errorMessage = null
      state.isFetching = true
    },
    risksLoadFailure(state, error) {
      state.errorMessage = error
      state.isFetching = false
    },
    riskList(state, risks) {
      state.riskList = risks
      state.isFetching = false
    }
  },
  actions: {
    loadRisks ({commit}) {
      commit('risksLoadStart')
      try {
        axios.get('risk').then((response) => {
        commit('riskList', response.data)
      })
    } catch (error) {
        commit('risksLoadFailure', error)
      }
    }
  }
});

new Vue({
  router,
  computed: Vuex.mapState(['risk', 'riskList', 'isFetching']),
  store,
  created() {
    this.$store.dispatch('loadRisks')
  },
  render: h => h(App)
}).$mount('#app')

