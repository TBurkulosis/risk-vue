# risk-vue

## Project setup
```
npm install
```

### cd into project root folder (risk-vue) then:

### Compiles and hot-reloads for development
```
npm run serve
```

### Project will be served at localhost:8080

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
