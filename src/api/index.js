import axios from 'axios'
//import router from '../router'


axios.defaults.baseURL = 'https://localhost:5001/api/'

// axios.defaults.baseURL = 'https://cammsriskapp.azurewebsites.net'
// axios.defaults.auth = {
// 	user: 'cammsrisk',
// 	password: 'cammsR!sk'
// }


export async function risks (method, data) {
	const opts = {
		method: method || 'GET',
		url: 'risk',
		data: data || {},
		WithCredentials: false,
		headers: {
			'accept': 'application/json',
			'contentType': 'application/json'
		}
	}

	if (data && data['id']) {
		opts.url = opts.url + '/' + data['id']
	}

	await axios(opts).then(function(response) {
		// eslint-disable-next-line
		console.log("api call response is:");
		// eslint-disable-next-line
		console.log(response);
		if (response.status >= 400) {
			throw response;
		}
		if (!response) {
			return undefined;
		}
		return response.data;
	});
}
export function newFunction() {
	return false;
}

