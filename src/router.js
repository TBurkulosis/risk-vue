import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/views/Layout'
import Home from './views/HomeWithKeys.vue'
import Search from './views/SearchWithKeys.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Risks',
      component: Layout,
      children: [
          {
            path: 'home',
            alias: '',
            name: 'Home',
            component: Home
          },
          {
            path: 'search',
            alias: '',
            name: 'Search',
            component: Search
          }
      ]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
